function sendRegisterNewsletter(mail,lang){
    jQuery.ajax({
        type:"POST",
        url:wpa_data.admin_ajax, 
        datatype:'json',
        data:{
            action:"registerNewsletter",
            "mail":mail,
            "lang":lang
        },
        success: function(data){
            console.log(data);
        }, 
        error: function (xhr, ajaxOptions, thrownError) {
        // this error case means that the ajax call, itself, failed, e.g., a syntax error
        // in your_function()
        alert ('Request failed: ' + thrownError.message) ;
        },
    });
}