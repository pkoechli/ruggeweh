jQuery(document).ready(function(){
    var offset = 500;
    var speed = 150;
    var duration = 500;
	   jQuery(window).scroll(function(){
            if (jQuery(this).scrollTop() < offset) {
			     jQuery('.backtotop') .fadeOut(duration);
            } else {
			     jQuery('.backtotop') .fadeIn(duration);
            }
        });
	jQuery('.backtotop').on('click', function(){
		jQuery('html, body').animate({scrollTop:0}, speed);
		return false;
		});
});
