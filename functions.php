<?php
pll_register_string('vimeo_header_1','vimeo_header_1','Rueckenschmerz');
pll_register_string('vimeo_header_2','vimeo_header_2','Rueckenschmerz');
pll_register_string( 'abschnitt_1_titel', 'Rückenschmerzen?', 'Rueckenschmerz');
pll_register_string( 'abschnitt_1_text', 'Rückenschmerzen sind typische Symptome für die Krankheit Morbus Bechterew. 80’000 Menschen in der Schweiz sind von Morbus Bechterew betroffen. Und Sie? Machen Sie den Online-Diagnosetest, um herauszufinden, ob bei Ihnen der Verdacht auf Morbus Bechterew besteht.', 'Rueckenschmerz');
pll_register_string('abschnitt_2_bechterew','Was ist Morbus Bechterew?','Rueckenschmerz');
pll_register_string('abschnitt_2_diagnosetest','Diagnosetest ausfüllen', 'Rueckenschmerz');
pll_register_string('abschnitt_3_titel','Die Krankheit Morbus Bechterew', 'Rueckenschmerz');
pll_register_string('abschnitt_3_blockheader_1','Morbus Bechterew ist eine chronische Rückenerkrankung', 'Rueckenschmerz');
pll_register_string('abschnitt_3_blocktext_1','Dies ist ein Test-Text.', 'Rueckenschmerz');
pll_register_string('abschnitt_3_video_url','https://www.youtube.com/embed/ZY1igCmc928?rel=0','Rueckenschmerz');
pll_register_string('abschnitt_3_blockheader_2','Typische Symptome von Bechterew', 'Rueckenschmerz');
pll_register_string('abschnitt_3_blocktext_2','Dies ist ein Test-Text. 2', 'Rueckenschmerz');
pll_register_string('abschnitt_3_blockheader_3','Lorem ipsum', 'Rueckenschmerz');
pll_register_string('abschnitt_3_blocktext_3','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.', 'Rueckenschmerz');
pll_register_string('abschnitt_4_zitat','"Bewegung hilft mir besonders am Morgen nach dem Aufwachen. Dank der Diagnose weiss ich nun, weshalb das so ist."', 'Rueckenschmerz');
pll_register_string('abschnitt_4_beschreibung', 'Frieda Mustermann, Bechterew diagnostiziert seit 2008.', 'Rueckenschmerz');
pll_register_string('abschnitt_5_titel', 'Den kostenlosen Diagnosetest ausfüllen und sofort Feedback erhalten', 'Rueckenschmerz');
pll_register_string('abschnitt_5_beschreibung', 'Vielleicht leiden Sie unter Rückenschmerzen, an entzündeten Augen oder an schlaflosen Nächten. Dies könnten Zeichen dafür sein, dass bei Ihnen der Verdacht auf «Bechterew» besteht.', 'Rueckenschmerz');
pll_register_string('Schritt 1','Schritt 1','Rueckenschmerz');
pll_register_string('Schritt 2','Schritt 2','Rueckenschmerz');
pll_register_string('Ergebnis','Ergebnis','Rueckenschmerz');
pll_register_string('Ja','Ja','Rueckenschmerz');
pll_register_string('Nein','Nein','Rueckenschmerz');
pll_register_string('Weiss nicht','Weiss nicht','Rueckenschmerz');
pll_register_string('Frage 1',"Nächtliche Kreuz- oder Rückenschmerzen oder eine versteifte Wirbelsäule am Morgen?",'Rueckenschmerz');
pll_register_string('Frage 2',"Einzelne geschwollene Gelenke?",'Rueckenschmerz');
pll_register_string('Frage 3',"Wurde im Monat davor auch schon eine Harnröhrenentzündung oder eine Gebärmutterhalsentzündung festgestellt?",'Rueckenschmerz');
pll_register_string('Frage 4',"Hatten Sie im Monat davor akuten Durchfall?",'Rueckenschmerz');
pll_register_string('Frage 5',"Schmerzen im Gesäss, einmal rechts, einmal links?",'Rueckenschmerz');
pll_register_string('Frage 6',"Vorübergehend einzelne wurstförmige Zehen oder Finger?",'Rueckenschmerz');
pll_register_string('Frage 7',"Schmerzen an den Fersen?",'Rueckenschmerz');
pll_register_string('Frage 8',"Regenbogenhautentzündung im Auge?",'Rueckenschmerz');
pll_register_string('Frage 9',"Eichelentzündung, Schuppenflechte oder chronische Darmentzündung?",'Rueckenschmerz');
pll_register_string('Frage 10',"Röntgenbilder/MRI haben gezeigt, dass meine Kreuz-Darmbein-Gelenke beidseitig entzündet sind?",'Rueckenschmerz');
pll_register_string('Frage 11',"Meine Schmerzen bessern, wenn ich nichtsteroidale Antirheumatika (Entzündungshemmer) einnehme und wenn ich die Medikamente absetze, kommen die Schmerzen zurück?",'Rueckenschmerz');
pll_register_string('Frage 12',"Man hat bei mir den Erbfaktor HLA-B27 nachgewiesen. Oder in meiner Familie ist eine der folgenden Krankheiten aufgetreten: Morbus Bechterew, Reiter-Syndrom, Regenbogenhautentzündung, Schuppenflechte oder eine chronische Darmentzündung?",'Rueckenschmerz');
pll_register_string('Ihre Angaben','Ihre Angaben','Rueckenschmerz');
pll_register_string('Name','Name','Rueckenschmerz');
pll_register_string('E-Mail','E-Mail','Rueckenschmerz');
pll_register_string('Name','Name','Rueckenschmerz');
pll_register_string('checkboxNewsletter','Gratis E-Mail-Newsletter abonnieren. 4x jährlich, kompakte Informationen zum Verein und zur Krankheit, direkt in Ihre Mailbox. Den Newsletter können Sie jederzeit wieder abbestellen.','Rueckenschmerz');
pll_register_string('Freiwillige Angaben','Ihre Angaben sind freiwillig. Sie werden vertraulich behandelt und niemals an Dritte weitergegeben.','Rueckenschmerz');
pll_register_string('Button Weiter','Weiter','Rueckenschmerz');
pll_register_string('Ihr Resultat','Ihr Resultat','Rueckenschmerz');
pll_register_string('Allgemeintext','Morbus Bechterew ist eine chronisch-rheumatische Erkrankung. Erste Symptome treten meist im Alter zwischen 15 und 35 Jahren auf. Bis zur Diagnose verstreichen jedoch meist rund sechs Jahre. Je früher die Diagnose gestellt werden kann, desto schneller können durch passende Therapien das Fortschreiten des Morbus Bechterews und somit die Schmerzen eingedämmt oder verhindert werden.','Rueckenschmerz');
pll_register_string('Bestellen','Bestellen Sie <a href="https://www.bechterew.ch/de/bestellformular.html">hier</a> die kostenlose Broschüre zu «Morbus Bechterew – Krankheit und Therapie».<br>Und erfahren Sie mehr über die <a href="https://www.bechterew.ch/de/was-wir-bieten.html">Vorteile einer Mitgliedschaft!</a>','Rueckenschmerz');
pll_register_string('Rheumatologen','<a href="http://www.rheuma-net.ch/Aerzteverzeichnis">Rheumatologen suchen</a>','Rueckenschmerz');
pll_register_string('KeinBechterew','Aufgrund dieses Tests haben Sie zur Zeit keinen Morbus Bechterew. Wir freuen uns für Sie und hoffen, dass es so bleibt.','Rueckenschmerz');
pll_register_string('VielleichtBechterew','Beobachten Sie ihre Beschwerden. Sollten diese zunehmen, raten wir Ihnen, sich von einer Fachperson untersuchen zu lassen.','Rueckenschmerz');
pll_register_string('BechterewJa','Es besteht der Verdacht, dass Sie an Morbus Bechterew oder an einer verwandten Krankheit leiden. Lassen Sie sich von einer Fachperson untersuchen.','Rueckenschmerz');
pll_register_string('Cleverreach-URL','http://31704.cleverreach.de/f/47566/wcs/','Rueckenschmerz');

//TODO: Add diagnosetest here
pll_register_string('abschnitt_6_titel','Ein trainierter Rücken ist wichtig und beugt Schmerzen vor','Rueckenschmerz');
pll_register_string('abschnitt_6_untertitel','Aus diesem Grund gibt es die neue Plattform mit Übungen gegen Morbus Bechterew.','Rueckenschmerz');
pll_register_string('abschnitt_6_button','Mehr zur Plattform auf rheumafit.ch','Rueckenschmerz');
pll_register_string('abschnitt_7_titel','Wir stärken uns den Rücken','Rueckenschmerz');
pll_register_string('abschnitt_7_untertitel','Betroffene erzählen, wie sie mit der Krankheit Morbus Bechterew umgehen.','Rueckenschmerz');
pll_register_string('abschnitt_7_card_untertitel_1',"«Sport war schon immer ein grosser Teil meines Lebens»",'Rueckenschmerz');
pll_register_string('abschnitt_7_card_text_1',"Sport war schon immer ein grosser Teil meines Lebens, und ich bin froh, dass ich dank der Bewegungstherapie und den Medikamenten so aktiv sein kann. Ich mache seit 2013 in der Therapiegruppe spezifisches Bechterew-Training. Ich habe ziemlich schnell damit angefangen, nachdem ich die Diagnose bekommen habe. Ich mache gerne Übungen, bei denen ich die Brustwirbelsäule rotieren muss. Denn hier bin ich leicht eingeschränkt. Zudem trainiere ich gerne mit dem Gymnastikball, um auch das Gleichgewicht und die Koordination zu fördern. Die spezifischen Übungen sind wirkungsvoll und die Betroffenen können davon profitieren.",'Rueckenschmerz');
pll_register_string('abschnitt_7_card_untertitel_2',"«Ich kann trotz Krankheit noch lachen»",'Rueckenschmerz');
pll_register_string('abschnitt_7_card_text_2',"Ich lebe seit fast 40 Jahren mit Morbus Bechterew. Am Anfang war ich natürlich sehr schockiert. Ich wusste ja auch nichts über diese Krankheit. Ich musste mich auf die Informationen einer einzigen Person, meiner Ärztin, verlassen. Sie teilte mir aber gleich mit, dass ich mit der Bechterew-Gymnastik anfangen müsse, wenn ich nicht mit 50 im Rollstuhl sein wolle. Auch wenn das ziemlich übertrieben war, gab es mir doch den richtigen Impuls für den Umgang mit der Krankheit. Nach dem Schock kam die zweite Phase im Umgang mit der Krankheit, der Abfindungsprozess. Dabei half mir der Austausch mit anderen Betroffenen sehr. Ich konnte bei ihnen sehen, dass sie dennoch ein relativ normales Leben führten und immer noch lachen konnten.",'Rueckenschmerz');
pll_register_string('abschnitt_7_card_untertitel_3',"«Mit Yoga, Meditation und einer gesunden Ernährung dem Bechterew entgegen treten»",'Rueckenschmerz');
pll_register_string('abschnitt_7_card_text_3','Als Kind konnte ich in meinem Zimmer stundenlang malen oder mich mit fantasievollen Geschichten beschäftigen. Mit 19 Jahren brach ich von meinem Heimatdorf nach Berlin auf, um «Digital Design» zu studieren. Mit dem Studium konnte ich die kreative Ader und das Marketing verbinden. Inzwischen habe ich auch eine Ausbildung zur Yogalehrerin absolviert und möchte dieses Wissen, verbunden mit einer positiven Einstellung, an andere Bechterew-Betroffene weitergeben. Mein ganzer Lebensstil richtet sich an möglichst viel Bewegung und einer gesunden Ernährung aus. So will ich der Krankheit gekonnt entgegen treten.','Rueckenschmerz');
pll_register_string('footer_kampagne',"Eine Kampagne der Schweizerischen Vereinigung Morbus Bechterew",'Rueckenschmerz');
pll_register_string('footer_bechterew_image', '/img/Bechterew_Logo_d_RGB_Grau.svg','Rueckenschmerz');
pll_register_string('footer_partner','Partner','Rueckenschmerz');
pll_register_string('footer_sponsoren','Sponsoren','Rueckenschmerz');
pll_register_string('back_to_top','Nach oben','Rueckenschmerz');
pll_register_string('startIndex','StartIndex','Rueckenschmerz');


function wpa_scripts() {
    wp_enqueue_script(
        'wpa_script',
        get_template_directory_uri() . '/js/newsletter.js',
        array('jquery'),
        null,
        true
    );
    $script_data = array(
        'admin_ajax' => admin_url( 'admin-ajax.php' )
    );
    wp_localize_script(
        'wpa_script',
        'wpa_data',
        $script_data
    );
}
add_action( 'wp_enqueue_scripts', 'wpa_scripts' );

    add_action('wp_ajax_registerNewsletter', 'registerNewsletter');
    add_action('wp_ajax_nopriv_registerNewsletter', 'registerNewsletter');
    function registerNewsletter(){
        include ('newsletter.php'); 
        if (isset($_POST['mail'])){
            $mail = $_POST['mail'];
            $lang = $_POST['lang'];
        
            $rest = new CR\tools\rest("https://rest.cleverreach.com/v2");
        
        
            echo "### Login - will retrieve Token ###\n";
            try {
                $token = $rest->post('/login',
                                    array(
                                        "client_id"=>'31704',
                                        "login"=>'admin',
                                        "password"=>'bechterew'
                                    )
                                    );
        
                $rest->setAuthMode("bearer", $token);
            }
            catch (\Exception $e){
                var_dump( (string) $e );
                var_dump($rest->error);
                exit;
            }
            try{
                if ($lang == "de"){
                $data =$rest->post("/groups/14313/receivers",array("email"=>$mail,"deactivated"=>1));
                $mailData = $rest->post("/forms/47566/send/activate",array(
                    "email"=>$mail,
                    "doidata" => array(
                        "user_ip"    => $_SERVER["REMOTE_ADDR"],
                        "referer"    => $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],
                        "user_agent" => $_SERVER["HTTP_USER_AGENT"]
                    )));
                }
                elseif($lang == "fr"){
                    $data =$rest->post("/groups/14323/receivers",array("email"=>$mail,"deactivated"=>1));
                $mailData = $rest->post("/forms/47596/send/activate",array(
                    "email"=>$mail,
                    "doidata" => array(
                        "user_ip"    => $_SERVER["REMOTE_ADDR"],
                        "referer"    => $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"],
                        "user_agent" => $_SERVER["HTTP_USER_AGENT"]
                    )));
                }
            }
            catch (\Exception $e){
                var_dump( (string) $e );
                var_dump($rest->error);
                exit;
            }
            var_dump($mailData);
        }
    }
?>
