<!DOCTYPE html>
<html>

<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-123610019-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-123610019-1');
</script>

    <meta name="viewport"content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet"href="<?php bloginfo('stylesheet_url'); ?>"type="text/css"media="screen"/>
    <link rel="pingback"href="<?php bloginfo('pingback_url'); ?>"/>
    <link rel="stylesheet"href="https://use.fontawesome.com/releases/v5.0.9/css/all.css"integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1"crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700"rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <?php wp_head(); ?>
    <title>Ruggeweh</title>
</head>

<body>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>

    <div class="container-fluid background blue header addAnglePaddingBottom">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-12">

                <div class="embed-responsive embed-responsive-16by9"id="firstVideo">
                    <iframe class="embed-responsive-item btn-cornered" data-src="<?php pll_e("vimeo_header_1"); ?>"webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
            <div class="w-100 d-md-none"></div>
            <div class="col-lg-4 col-12 d-none d-lg-block">

                <div class="embed-responsive embed-responsive-16by9"id="secondVideo">&nbsp;</div>
            </div>
            <div class="w-100 addPaddingTop"></div>

            <div class="col"></div>
            <div class="col-lg-6 col-12">
                <h4 class="text-center">
                    <?php pll_e("Rückenschmerzen?"); ?>
                </h4>
                <p class="text-center">
                    <?php pll_e("Rückenschmerzen sind typische Symptome für die Krankheit Morbus Bechterew. 80’000 Menschen in der Schweiz sind von Morbus Bechterew betroffen. Und Sie? Machen Sie den Online-Diagnosetest, um herauszufinden, ob bei Ihnen der Verdacht auf Morbus Bechterew besteht."); ?>
                </p>
            </div>
            <div class="col"></div>
        </div>
    </div>
    <div class="container-fluid noPadding">
        <div class="angle">
            <svg class="angle-separator angle-top greyAngle"xmlns="http://www.w3.org/2000/svg"version="1.1"fill="#fff"width="100%"height="70"viewBox="0 0 4 0.266661"preserveAspectRatio="none"style="">
                    <polygon class="fil0"points="4,0 4,0.26661 -0,0.26661"></polygon>
                </svg>
        </div>
    </div>
    <div class="container-fluid background grey addAnglePaddingBottom addPaddingTopQuarter">
        <div class="row justify-content-center">
            <div class="col d-none d-lg-block"></div>
            <div class="col-lg-3 col-5 text-center">
                <a href="#wasistbechterew">
                        <img data-src="<?php echo get_template_directory_uri(); ?>/img/icon_wasistmorbusbechterew.svg"class="icons">
                        <p class="addPaddingTopHalf"><strong><?php pll_e("Was ist Morbus Bechterew?"); ?></strong></p>
                    </a>
            </div>
            <div class="vertical-divider white"></div>
            <div class="col-lg-3 col-5 text-center">
                <a href="#diagnosetest">
                        <img data-src="<?php echo get_template_directory_uri(); ?>/img/icon_diagnosetest.svg"class="icons">
                        <p class="addPaddingTopHalf"><strong><?php pll_e("Diagnosetest ausfüllen"); ?></strong></p>
                    </a>
            </div>
            <div class="col d-none d-lg-block"></div>
        </div>
    </div>
    <div class="container-fluid noPadding">
        <div class="angle">
            <svg class="angle-separator angle-top whiteAngle"xmlns="http://www.w3.org/2000/svg"version="1.1"fill="#fff"width="100%"height="70"viewBox="0 0 4 0.266661"preserveAspectRatio="none"style="">
                    <polygon class="fil0"points="4,0 4,0.26661 -0,0.26661"></polygon>
                </svg>
        </div>
    </div>
    <div class="container-fluid background white"id="wasistbechterew">
        <div class="row justify-content-center">
            <div class="col"></div>
            <div class="col-lg-5 col-12">
                <h4 class="text-center">
                    <?php pll_e("Die Krankheit Morbus Bechterew"); ?>
                </h4>

            </div>
            <div class="col"></div>
        </div>
        <div class="row justify-content-center addPaddingTopHalf">
            <div class="col"></div>
            <div class="col-lg-5 col-12">
                <div class="card card-collapse">
                    <a data-toggle="collapse"href="#block1"aria-expanded="true"aria-controls="block1">
                        <div class="card-header">
                            <i class="fas fa-plus addPaddingRight"id="plusblock1"></i>
                            <?php pll_e("Morbus Bechterew ist eine chronische Rückenerkrankung"); ?>
                        </div>
                    </a>
                    <div id="block1"class="collapse">
                        <div class="card-block">
                            <?php pll_e("Dies ist ein Test-Text."); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>
        <div class="row justify-content-center addPaddingTopHalf">
            <div class="col"></div>
            <div class="col-lg-5 col-12">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item btn-cornered"data-src="<?php pll_e("https://www.youtube.com/embed/ZY1igCmc928?rel=0"); ?>"allowfullscreen="">&lt;br /&gt;
                        </iframe>
                </div>
            </div>
            <div class="col"></div>
        </div>
        <div class="row justify-content-center addPaddingTopHalf">
            <div class="col"></div>
            <div class="col-lg-5 col-12">
                <div class="card card-collapse">
                    <a data-toggle="collapse"href="#block2"aria-expanded="true"aria-controls="block2">
                        <div class="card-header">
                            <i class="fas fa-plus addPaddingRight"id="plusblock2"></i>
                            <?php pll_e("Typische Symptome von Bechterew"); ?>
                        </div>
                    </a>
                    <div id="block2"class="collapse">
                        <div class="card-block">
                            <?php pll_e("Dies ist ein Test-Text. 2"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>
        <div class="row justify-content-center addPaddingTopQuarter addPaddingBottomQuarter">
            <div class="col"></div>
            <div class="col-lg-5 col-12">
                <div class="card card-collapse">
                    <a data-toggle="collapse"href="#block3"aria-expanded="true"aria-controls="block3">
                        <div class="card-header">
                            <i class="fas fa-plus addPaddingRight"id="plusblock3"></i>
                            <?php pll_e("Lorem ipsum"); ?>
                        </div>
                    </a>
                    <div id="block3"class="collapse">
                        <div class="card-block">
                            <?php pll_e("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>

    </div>
    <div class="angle noPadding">
        <svg class="angle-separator angle-bottom whiteAngle"xmlns="http://www.w3.org/2000/svg"version="1.1"fill="#fff"width="100%"height="70"viewBox="0 0 4 0.266661"preserveAspectRatio="none"style="">
                <polygon class="fil0"points="0,0 4,0 -0,0.26661"></polygon>
            </svg>
    </div>
    <div class="container-fluid background blue parallax addAnglePaddingTop addAnglePaddingBottom">
        <div class="row justify-content-center">
            <div class="col"></div>
            <div class="col-lg-8 col-12">
                <h1 class="text-center">
                    <?php pll_e('"Bewegung hilft mir besonders am Morgen nach dem Aufwachen. Dank der Diagnose weiss ich nun, weshalb das so ist."'); ?>
                </h1>
                <p class="text-center addPaddingTopHalf">
                    <?php pll_e("Frieda Mustermann, Bechterew diagnostiziert seit 2008."); ?>
                </p>
            </div>
            <div class="col"></div>
        </div>
    </div>
    <div class="container-fluid noPadding">
        <div class="angle">
            <svg class="angle-separator angle-top greyAngle"xmlns="http://www.w3.org/2000/svg"version="1.1"fill="#fff"width="100%"height="70"viewBox="0 0 4 0.266661"preserveAspectRatio="none"style="">
                    <polygon class="fil0"points="4,0 4,0.26661 -0,0.26661"></polygon>
                </svg>
        </div>
    </div>
    <div class="container-fluid background grey addPaddingTopQuarter addPaddingBottomQuarter">
        <div class="row justify-content-center"id="diagnosetest">
            <div class="col"></div>
            <div class="col-lg-6 col-12">
                <h4 class="text-center">
                    <?php pll_e("Den kostenlosen Diagnosetest ausfüllen und sofort Feedback erhalten"); ?>
                </h4>
                <p class="text-center">
                    <?php pll_e('Vielleicht leiden Sie unter Rückenschmerzen, an entzündeten Augen oder an schlaflosen Nächten. Dies könnten Zeichen dafür sein, dass bei Ihnen der Verdacht auf «Bechterew» besteht.'); ?>
                </p>
                <div class="form white addPaddingTop"id="diagnoseForm">
                    <div class="row diagnosetest">
                        <div id="step1"class="col-lg-2 col-4 testheader">

                            <?php pll_e('Schritt 1'); ?>

                            <hr class="bechterewblau"/>

                        </div>
                        <div id="step2"class="col-lg-2 col-4 testheader">

                            <?php pll_e('Schritt 2'); ?>

                            <hr />

                        </div>
                        <div id="ergebnisbanner"class="col-lg-2 col-4 testheader">

                            <?php pll_e('Ergebnis'); ?>

                            <hr />

                        </div>
                    </div>
                    <fieldset id="page1"class="diagnosetest">
                        <div id="question1"><strong><?php pll_e("Nächtliche Kreuz- oder Rückenschmerzen oder eine versteifte Wirbelsäule am Morgen?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage1"type="radio"value="1"id="frage1_1"/>
                                    <label class="custom-control-label"for="frage1_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage1"type="radio"value="2"id="frage1_2"/>
                                    <label class="custom-control-label"for="frage1_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage1"type="radio"value="3"id="frage1_3"/>
                                    <label class="custom-control-label"for="frage1_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>
                        <div id="question2"><strong><?php pll_e("Einzelne geschwollene Gelenke?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage2"type="radio"value="1"id="frage2_1"/>
                                    <label class="custom-control-label"for="frage2_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage2"type="radio"value="2"id="frage2_2"/>
                                    <label class="custom-control-label"for="frage2_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage2"type="radio"value="3"id="frage2_3"/>
                                    <label class="custom-control-label"for="frage2_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>
                        <div id="question3"><strong><?php pll_e("Wurde im Monat davor auch schon eine Harnröhrenentzündung oder eine Gebärmutterhalsentzündung festgestellt?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage3"type="radio"value="1"id="frage3_1"/>
                                    <label class="custom-control-label"for="frage3_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage3"type="radio"value="2"id="frage3_2"/>
                                    <label class="custom-control-label"for="frage3_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage3"type="radio"value="3"id="frage3_3"/>
                                    <label class="custom-control-label"for="frage3_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>
                        <div id="question4"><strong><?php pll_e("Hatten Sie im Monat davor akuten Durchfall?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage4"type="radio"value="1"id="frage4_1"/>
                                    <label class="custom-control-label"for="frage4_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage4"type="radio"value="2"id="frage4_2"/>
                                    <label class="custom-control-label"for="frage4_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage4"type="radio"value="3"id="frage4_3"/>
                                    <label class="custom-control-label"for="frage4_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>
                        <div id="question5"><strong><?php pll_e("Schmerzen im Gesäss, einmal rechts, einmal links?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage5"type="radio"value="1"id="frage5_1"/>
                                    <label class="custom-control-label"for="frage5_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage5"type="radio"value="2"id="frage5_2"/>
                                    <label class="custom-control-label"for="frage5_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage5"type="radio"value="3"id="frage5_3"/>
                                    <label class="custom-control-label"for="frage5_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>
                        <div id="question6"><strong><?php pll_e("Vorübergehend einzelne wurstförmige Zehen oder Finger?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage6"type="radio"value="1"id="frage6_1"/>
                                    <label class="custom-control-label"for="frage6_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage6"type="radio"value="2"id="frage6_2"/>
                                    <label class="custom-control-label"for="frage6_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage6"type="radio"value="3"id="frage6_3"/>
                                    <label class="custom-control-label"for="frage6_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>
                        <div id="question7"><strong><?php pll_e("Schmerzen an den Fersen?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage7"type="radio"value="1"id="frage7_1"/>
                                    <label class="custom-control-label"for="frage7_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage7"type="radio"value="2"id="frage7_2"/>
                                    <label class="custom-control-label"for="frage7_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage7"type="radio"value="3"id="frage7_3"/>
                                    <label class="custom-control-label"for="frage7_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>

                        <div id="question8"><strong><?php pll_e("Regenbogenhautentzündung im Auge?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage8"type="radio"value="1"id="frage8_1"/>
                                    <label class="custom-control-label"for="frage8_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage8"type="radio"value="2"id="frage8_2"/>
                                    <label class="custom-control-label"for="frage8_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage8"type="radio"value="3"id="frage8_3"/>
                                    <label class="custom-control-label"for="frage8_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>

                        <div id="question9"><strong><?php pll_e("Eichelentzündung, Schuppenflechte oder chronische Darmentzündung?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage9"type="radio"value="1"id="frage9_1"/>
                                    <label class="custom-control-label"for="frage9_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage9"type="radio"value="2"id="frage9_2"/>
                                    <label class="custom-control-label"for="frage9_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage9"type="radio"value="3"id="frage9_3"/>
                                    <label class="custom-control-label"for="frage9_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>

                        <div id="question10"><strong><?php pll_e("Röntgenbilder/MRI haben gezeigt, dass meine Kreuz-Darmbein-Gelenke beidseitig entzündet sind?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage10"type="radio"value="1"id="frage10_1"/>
                                    <label class="custom-control-label"for="frage10_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage10"type="radio"value="2"id="frage10_2"/>
                                    <label class="custom-control-label"for="frage10_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage10"type="radio"value="3"id="frage10_3"/>
                                    <label class="custom-control-label"for="frage10_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>

                        <div id="question11"><strong><?php pll_e("Meine Schmerzen bessern, wenn ich nichtsteroidale Antirheumatika (Entzündungshemmer) einnehme und wenn ich die Medikamente absetze, kommen die Schmerzen zurück?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage11"type="radio"value="1"id="frage11_1"/>
                                    <label class="custom-control-label"for="frage11_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage11"type="radio"value="2"id="frage11_2"/>
                                    <label class="custom-control-label"for="frage11_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage11"type="radio"value="3"id="frage11_3"/>
                                    <label class="custom-control-label"for="frage11_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>

                        <div id="question12"><strong><?php pll_e("Man hat bei mir den Erbfaktor HLA-B27 nachgewiesen. Oder in meiner Familie ist eine der folgenden Krankheiten aufgetreten: Morbus Bechterew, Reiter-Syndrom, Regenbogenhautentzündung, Schuppenflechte oder eine chronische Darmentzündung?"); ?></strong><br>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage12"type="radio"value="1"id="frage12_1"/>
                                    <label class="custom-control-label"for="frage12_1"><?php pll_e("Ja"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage12"type="radio"value="2"id="frage12_2"/>
                                    <label class="custom-control-label"for="frage12_2"><?php pll_e("Nein"); ?></label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input"name="frage12"type="radio"value="3"id="frage12_3"/>
                                    <label class="custom-control-label"for="frage12_3"><?php pll_e("Weiss nicht"); ?></label>
                                </div>
                            </div>
                        </div>

                        <a id="goToPage2"class="btn btn-primary float-right"href="#diagnoseForm">
                            <?php pll_e('Weiter'); ?>
                        </a>
                    </fieldset>

                    <fieldset id="page2"class="hidden">
                        <!-- Here comes the mail and the name -->
                        <!-- TODO Looks wrong -->
                        <h3><?php pll_e("Ihre Angaben"); ?></h3>
                        <div class="col-6 noPadding">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"id="addon1"><?php pll_e('Name'); ?></span>
                                </div>
                                <input id="name"class="form-control"name="name"type="text"/>
                            </div>
                        </div>
                        <div class="w-100"></div>
                        <form id="mail-form"class="form-group">
                            <div class="cr_body cr_page cr_font formbox col-6 noPadding">
                                <div class="editable_content">
                                    <div id="1069672"class="cr_ipe_item ui-sortable musthave">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"id="addon2"><?php pll_e('E-Mail'); ?></span>
                                            </div>
                                            <input id="text1069672"class="form-control"name="email"type="text"value=""/>

                                        </div>
                                    </div>
                                </div>
                                <noscript>&nbsp;</noscript>
                            </div>
                            <div class="checkbox"><label>
                                    <input id="chkNewsletter"type="checkbox"value=""/>&nbsp;<?php pll_e('Gratis E-Mail-Newsletter abonnieren. 4x jährlich, kompakte Informationen zum Verein und zur Krankheit, direkt in Ihre Mailbox. Den Newsletter können Sie jederzeit wieder abbestellen.'); ?>
                                    </label></div>
                            <p class="small">
                                <?php pll_e('Ihre Angaben sind freiwillig. Sie werden vertraulich behandelt und niemals an Dritte weitergegeben.'); ?>
                            </p>
                            <input name="ok"type="hidden"value="1"/>
                            <input id="dbresp"name="dbresp"type="hidden"value="MorbusNein"/>
                            <button class="btn btn-primary pull-right"><?php pll_e('Weiter'); ?></button>

                        </form>


                    </fieldset>

                    <fieldset id="ergebnis"class="hidden diagnosetest">
                        <h3>
                            <?php pll_e('Ihr Resultat'); ?>
                        </h3>
                        <div id="ergebnistext"><strong><p>&nbsp;</p></strong></div>

                        <hr />

                        <div id="allgemeintext">
                            <p class="small">
                                <?php pll_e('<a href="http://www.rheuma-net.ch/Aerzteverzeichnis">Rheumatologen suchen</a>'); ?></p>
                            <p class="small">
                                <?php pll_e('Morbus Bechterew ist eine chronisch-rheumatische Erkrankung. Erste Symptome treten meist im Alter zwischen 15 und 35 Jahren auf. Bis zur Diagnose verstreichen jedoch meist rund sechs Jahre. Je früher die Diagnose gestellt werden kann, desto schneller können durch passende Therapien das Fortschreiten des Morbus Bechterews und somit die Schmerzen eingedämmt oder verhindert werden.'); ?>
                            </p>
                            <p class="small">
                                <?php pll_e('Bestellen Sie <a href="https://www.bechterew.ch/de/bestellformular.html">hier</a> die kostenlose Broschüre zu «Morbus Bechterew – Krankheit und Therapie».<br>Und erfahren Sie mehr über die <a href="https://www.bechterew.ch/de/was-wir-bieten.html">Vorteile einer Mitgliedschaft!</a>'); ?></p>

                        </div>
                    </fieldset>

                </div>
            </div>
            <div class="col"></div>

        </div>

    </div>
    <div class="angle noPadding">
        <svg class="angle-separator angle-bottom greyAngle"xmlns="http://www.w3.org/2000/svg"version="1.1"fill="#fff"width="100%"height="70"viewBox="0 0 4 0.266661"preserveAspectRatio="none"style="">
                <polygon class="fil0"points="0,0 4,0  -0,0.26661"></polygon>
            </svg>
    </div>
    <div class="container-fluid background blue uebung addAnglePaddingTop addAnglePaddingBottom">
        <div class="row justify-content-center">
            <div class="col"></div>
            <div class="col-lg-10 col-12">
                <h1 class="text-center">
                    <?php pll_e('Ein trainierter Rücken ist wichtig und beugt Schmerzen vor'); ?>
                </h1>
                <p class="text-center addPaddingTopHalf">
                    <?php pll_e('Aus diesem Grund gibt es die neue Plattform mit Übungen gegen Morbus Bechterew.'); ?>
                </p>
                <div class="text-center">
                    <a href="https://www.rheumafit.ch"class="btn btn-flat">
                        <?php pll_e('Mehr zur Plattform auf rheumafit.ch'); ?>
                    </a>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
    <div class="container-fluid noPadding">
        <div class="angle">
            <svg class="angle-separator angle-top whiteAngle"xmlns="http://www.w3.org/2000/svg"version="1.1"fill="#fff"width="100%"height="70"viewBox="0 0 4 0.266661"preserveAspectRatio="none"style="">
                    <polygon class="fil0"points="4,0 4,0.26661 -0,0.26661"></polygon>
                </svg>
        </div>
    </div>
    <div class="container-fluid background white addAnglePaddingBottom addPaddingTopQuarter">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-12 text-center">
                <h3>
                    <?php pll_e("Wir stärken uns den Rücken"); ?>
                </h3>
                <p>
                    <?php pll_e("Betroffene erzählen, wie sie mit der Krankheit Morbus Bechterew umgehen."); ?>
                </p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-2"></div>
            <div class="col">
                <div class="card">
                    <img class="card-img-top"src="<?php echo get_template_directory_uri(); ?>/img/gretener.jpg"alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><strong>Manuela Gretener</strong></h5>
                        <h6><strong><?php pll_e("«Sport war schon immer ein grosser Teil meines Lebens»"); ?></strong></h6>
                        <p class="card-text">
                            <?php pll_e("Sport war schon immer ein grosser Teil meines Lebens, und ich bin froh, dass ich dank der Bewegungstherapie und den Medikamenten so aktiv sein kann. Ich mache seit 2013 in der Therapiegruppe spezifisches Bechterew-Training. Ich habe ziemlich schnell damit angefangen, nachdem ich die Diagnose bekommen habe. Ich mache gerne Übungen, bei denen ich die Brustwirbelsäule rotieren muss. Denn hier bin ich leicht eingeschränkt. Zudem trainiere ich gerne mit dem Gymnastikball, um auch das Gleichgewicht und die Koordination zu fördern. Die spezifischen Übungen sind wirkungsvoll und die Betroffenen können davon profitieren."); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="w-100 addPaddingTop d-md-none"></div>

            <div class="col">
                <div class="card">
                    <img class="card-img-top"src="<?php echo get_template_directory_uri(); ?>/img/hochstrasser.jpg"alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><strong>Andreas Hochstrasser</strong></h5>
                        <h6><strong><?php pll_e("«Ich kann trotz Krankheit noch lachen»"); ?></strong></h6>
                        <p class="card-text">
                            <?php pll_e("Ich lebe seit fast 40 Jahren mit Morbus Bechterew. Am Anfang war ich natürlich sehr schockiert. Ich wusste ja auch nichts über diese Krankheit. Ich musste mich auf die Informationen einer einzigen Person, meiner Ärztin, verlassen. Sie teilte mir aber gleich mit, dass ich mit der Bechterew-Gymnastik anfangen müsse, wenn ich nicht mit 50 im Rollstuhl sein wolle. Auch wenn das ziemlich übertrieben war, gab es mir doch den richtigen Impuls für den Umgang mit der Krankheit. Nach dem Schock kam die zweite Phase im Umgang mit der Krankheit, der Abfindungsprozess. Dabei half mir der Austausch mit anderen Betroffenen sehr. Ich konnte bei ihnen sehen, dass sie dennoch ein relativ normales Leben führten und immer noch lachen konnten."); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="w-100 addPaddingTop d-md-none"></div>

            <div class="col">
                <div class="card">
                    <img class="card-img-top"src="<?php echo get_template_directory_uri(); ?>/img/weins.jpg"alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><strong>Katharina Weins</strong></h5>
                        <h6><strong><?php pll_e("«Mit Yoga, Meditation und einer gesunden Ernährung dem Bechterew entgegen treten»"); ?></strong></h6>
                        <p class="card-text">
                            <?php pll_e('Als Kind konnte ich in meinem Zimmer stundenlang malen oder mich mit fantasievollen Geschichten beschäftigen. Mit 19 Jahren brach ich von meinem Heimatdorf nach Berlin auf, um «Digital Design» zu studieren. Mit dem Studium konnte ich die kreative Ader und das Marketing verbinden. Inzwischen habe ich auch eine Ausbildung zur Yogalehrerin absolviert und möchte dieses Wissen, verbunden mit einer positiven Einstellung, an andere Bechterew-Betroffene weitergeben. Mein ganzer Lebensstil richtet sich an möglichst viel Bewegung und einer gesunden Ernährung aus. So will ich der Krankheit gekonnt entgegen treten.'); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
    </div>
    <div class="container-fluid background grey">
        <div class="row justify-content-center">
            <div class="col">
            </div>
            <div class="col-lg-6 col-12 text-center minusMargin social">
                <a href="https://www.facebook.com/bechterew.ch/">
                        <span class="fa-stack fa-2x">
                            <i class="fas fa-circle fa-stack-2x grey"></i>
                            <i class="fab fa-facebook-f fa-stack-1x white"></i>
                        </span>
                    </a>
                <a href="https://www.instagram.com/bechterew.ch/">
                        <span class="fa-stack fa-2x">
                            <i class="fas fa-circle fa-stack-2x grey"></i>
                            <i class="fab fa-instagram fa-stack-1x white"></i>
                        </span>
                    </a>
                <a href="https://www.youtube.com/channel/UCk24ceIuHy-U1xJnexYcHhA">
                        <span class="fa-stack fa-2x">
                            <i class="fas fa-circle fa-stack-2x grey"></i>
                            <i class="fab fa-youtube fa-stack-1x white"></i>
                        </span>
                    </a>
            </div>

            <div class="col"></div>
        </div>
        <div class="row addPaddingTop justify-content-start addPaddingBottom">
            <div class="col"></div>
            <div class="col-lg-8 col-12">


                <div class="row noPadding">
                    <div class="col-lg-6 col-12">
                        <p class="small-text"><strong><?php pll_e("Eine Kampagne der Schweizerischen Vereinigung Morbus Bechterew"); ?></strong></p>
                        <a href="https://www.bechterew.ch">
                                <img data-src="<?php echo get_template_directory_uri(); ?><?php pll_e('/img/Bechterew_Logo_d_RGB_Grau.svg'); ?>"class="sponsorImage img-fluid"alt="Morbus Bechterew">
                            </a></div>
                    <div class="w-100 addPaddingTop d-md-none"></div>

                    <div class="col-lg-3 col-6">
                        <p><strong class="small-text"><?php pll_e("Partner"); ?></strong></p>
                        <a href="https://www.rheumaliga.ch/">
                                <img data-src="<?php echo get_template_directory_uri(); ?>/img/Rheumaliga_RGB_Grau.svg"class="sponsorImage medium float-left"alt="Rheumaliga"/>
                            </a>
                    </div>
                    <div class="col-lg-3 col-6">
                        <p><strong class="small-text"><?php pll_e("Sponsoren"); ?></strong></p>
                        <a href="http://www.msd.ch">
                                <img data-src="<?php echo get_template_directory_uri(); ?>/img/msd_grau_RGB.svg"class="sponsorImage small float-left"alt="Merck Sharp & Dohme AG">
                            </a>
                    </div>
                </div>
            </div>
            <div class="col"></div>
        </div>
    </div>
    <!-- PHP Post Code -->
    <?php 
    $startIndex = (int)pll__("StartIndex");
    echo "This".$startIndex;
    function writeToNewFileAndCopy($rowIndex){
    $diagnoseFile = get_template_directory().'/Diagnoseresultat.csv';
    $diagnoseFile_old = get_template_directory().'/Diagnose_old.csv';
    $temporaryFileName = get_template_directory()."/temporary_diagnosis.csv";
    $today = date("d.m.Y");
    if (file_exists($diagnoseFile)){
        $csvData = array_map(function($v){return str_getcsv($v,";");}, file($diagnoseFile));
        $temporaryFile = fopen($temporaryFileName,"w+");
        $dataCount = count($csvData);
        
        $date = $csvData[$dataCount-1][0];
        if ($date == $today){
            $csvData[$dataCount-1][$rowIndex]+=1;
            foreach ($csvData as $row){
                fputcsv($temporaryFile,$row,";");
            }
        }
        else{
            foreach($csvData as $row){
                fputcsv($temporaryFile,$row,";");
            }
            $newRowData = array($today,0,0,0,0);
            $newRowData[$rowIndex] = 1;
            fputcsv($temporaryFile, $newRowData,";");
        }
        fclose($temporaryFile);
        rename($diagnoseFile, $diagnoseFile_old);
        rename($temporaryFileName,$diagnoseFile);
    }
    else{
        $createCSVFile = fopen($diagnoseFile,"r+");
        $header = array("Datum","Deutsch Test","Deutsch Verdacht","Franzoesisch Test","Franzoesisch Verdacht");
        $rowData = array($today,0,0,0,0);
        $rowData[$rowIndex]=1;

        fputcsv($createCSVFile,$header,";");
        fputcsv($createCSVFile,$rowData,";");
        fclose($createCSVFile);
    }
}
if (isset($_POST['hasStarted'])){
    writeToNewFileAndCopy($startIndex);
}    

if (isset($_POST['dbResp'])){
     $dbResp = $_POST['dbResp'];
     if ($dbResp=="MorbusJa"){
      writeToNewFileAndCopy($startIndex+1);
    }
}
?>
    <script src="https://player.vimeo.com/api/player.js"></script>
    <script>
        var resultattext ="";
        jQuery(document).ready(function() {
            var allQuestions = jQuery('div[id^="question"]');
            jQuery("#goToPage2").addClass('disabled');

            

            var links = jQuery("a[href^='http']");
            jQuery(links).attr('target', '_blank');
            if (jQuery(window).width() > 992) {
                if (jQuery("#secondVideo").html().length<10){
                    jQuery("#secondVideo").html('<iframe class="embed-responsive-item btn-cornered" src="<?php pll_e("vimeo_header_2"); ?>" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                }
            }

            [].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
                img.setAttribute('src', img.getAttribute('data-src'));
                img.onload = function() {
                    img.removeAttribute('data-src');
                };
            });

            [].forEach.call(document.querySelectorAll('iframe[data-src]'), function(iframe) {
                iframe.setAttribute('src', iframe.getAttribute('data-src'));
                iframe.onload = function() {
                    iframe.removeAttribute('data-src');
                    var path = window.location.pathname;
                    if (path =="/fr/") {

                        var player = new Vimeo.Player(iframe);

                        player.enableTextTrack('fr').then(function(track) {}).catch(function(error) {
                            switch (error.name) {
                                case 'InvalidTrackLanguageError':
                                    // no track was available with the specified language
                                    break;

                                case 'InvalidTrackError':
                                    // no track was available with the specified language and kind
                                    break;

                                default:
                                    // some other error occurred
                                    break;
                            }

                        })

                    }
                };
            });
        });
        window.addEventListener("resize", function() {
            if (jQuery(window).width() > 992) {
                if (jQuery('#secondVideo').html().length<10){
                    jQuery("#secondVideo").html(' <iframe class="embed-responsive-item btn-cornered" src="<?php pll_e("vimeo_header_2"); ?>" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                    [].forEach.call(document.querySelectorAll('iframe[data-src]'), function(iframe) {
                        iframe.setAttribute('src', iframe.getAttribute('data-src'));
                        iframe.onload = function() {
                            iframe.removeAttribute('data-src');
                            var path = window.location.pathname;
                            if (path =="/fr/") {
                                var player = new Vimeo.Player(iframe);

                                player.enableTextTrack('fr').then(function(track) {}).catch(function(error) {
                                    switch (error.name) {
                                        case 'InvalidTrackLanguageError':
                                            // no track was available with the specified language
                                            break;

                                        case 'InvalidTrackError':
                                            // no track was available with the specified language and kind
                                            break;

                                        default:
                                            // some other error occurred
                                            break;
                                    }

                                })

                            }
                        };

                    });
                }
            }

        }, false);



        
        jQuery("#page1 input").on("change", function() {

            var fragen = jQuery("#page1").find('input[name^="frage"]');
            var allChecked = true;
            var goToPage2 = jQuery("#goToPage2");
            for (var k = 0; k < fragen.length; k += 3) {
                var inputName = jQuery(fragen[k]).attr('name');
                if (jQuery('input[name=' + inputName + ']:checked').length == 0) {
                    allChecked = false;
                    break;
                }
            }
            if (allChecked) {
                //Activate next button
                goToPage2.removeClass("disabled")
            } else {
                if (!goToPage2.hasClass("disabled")) {
                    goToPage2.addClass("disabled");
                }
            }
        });
        jQuery("#goToPage2").on('click', function() {
            jQuery.ajax({
                        data:{'hasStarted':'true'},
                        type: 'post',
                        success: function(result){},
                    });
            var page1 = jQuery("#page1");
            var page2 = jQuery("#page2");


            var fragen = jQuery("#page1").find('input[name^="frage"]:checked');
            var points = 0;
            for (var k=0;k<fragen.length;k++){
                var selectedValue = jQuery(fragen[k]).val();
                if (selectedValue!=2){
                    points++;
                }
            }

            var dbRespField = jQuery("#dbresp");
            if(points<=1){
                resultatText = "<?php pll_e("Aufgrund dieses Tests haben Sie zur Zeit keinen Morbus Bechterew. Wir freuen uns für Sie und hoffen, dass es so bleibt."); ?>";
                dbRespField.val("MorbusNein")
            }
            else if (points<6){
                resultatText="<?php pll_e("Beobachten Sie ihre Beschwerden. Sollten diese zunehmen, raten wir Ihnen, sich von einer Fachperson untersuchen zu lassen."); ?>";
                dbRespField.val("MorbusVielleicht")
            }
            else{
                resultatText="<?php pll_e("Es besteht der Verdacht, dass Sie an Morbus Bechterew oder an einer verwandten Krankheit leiden. Lassen Sie sich von einer Fachperson untersuchen."); ?>";
                dbRespField.val("MorbusJa");
            }

            page1.animate({ opacity: 0 },600);
            page1.hide();

            page2.css({
                visibility : 'visible',
                opacity : 0
            });
            page2.removeClass("hidden");
            page2.animate({opacity:1},600);

            jQuery("#step1 hr").removeClass("bechterewblau");
            jQuery("#step2 hr").addClass("bechterewblau");

        });
        jQuery("#mail-form").on('submit',function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            var sendNewsletter = jQuery("#chkNewsletter");

            if (sendNewsletter.is(':checked')){
                var mailAdress = jQuery("#text1069672");
                if (mailAdress.val()!=undefined){
                    var mail = mailAdress.val();
                    var lang = window.location.pathname;
                    if (lang.indexOf("fr")===-1){
                        lang="de";
                    }
                    else{
                        lang="fr";
                    }
                        sendRegisterNewsletter(mail,lang);
                    }
                }



            var page2 = jQuery("#page2");

            var ergebnistext = jQuery("#ergebnistext p");
            ergebnistext.text(resultatText);

            page2.animate({opacity:0},600);
            page2.hide();

            var ergebnis = jQuery("#ergebnis");
            ergebnis.css({visibility:'visible',opacity:0});
            ergebnis.removeClass("hidden");
            ergebnis.animate({opacity:1},600);

            jQuery("#step2 hr").removeClass("bechterewblau");
            jQuery("#ergebnisbanner hr").addClass("bechterewblau");


            var dbRespValue = jQuery("#dbresp").val();
            jQuery.ajax({
                data:{'dbResp':dbRespValue},
                type: 'post',
                success:function(result){
            }
            });
        });


        // Select all links with hashes
        jQuery('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .not('[href^="#carousel"]')
            .not('[href^="#block"]')
            .click(function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = jQuery(this.hash);
                    target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        jQuery('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var jQuerytarget = jQuery(target);
                            jQuerytarget.focus();
                            if (jQuerytarget.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                jQuerytarget.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                jQuerytarget.focus(); // Set focus again
                            };
                        });
                    }
                }
            });
        jQuery('div[id^="block"]').on('shown.bs.collapse', function() {
            var collapseID = jQuery(this).attr('id');
            var plusSign = jQuery("#plus"+ collapseID);
            plusSign.removeClass("fa-plus");
            plusSign.addClass("fa-minus");
        });
        jQuery('div[id^="block"]').on('hidden.bs.collapse', function() {
            var collapseID = jQuery(this).attr('id');
            var plusSign = jQuery("#plus"+ collapseID);
            plusSign.addClass("fa-plus");
            plusSign.removeClass("fa-minus");
        });

    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"crossorigin="anonymous"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/backtotop.js"></script>
   <a href="#"class="btn btn-primary backtotop"><i class="fas fa-angle-up"></i>&nbsp;&nbsp;<?php pll_e('Nach oben'); ?></a>
    <?php wp_footer(); ?>
</body>

</html>
